# TGCOncallTools

## BigStorm
 

## C05TriggerHole
<dl>
  <dt>C05TriggerHole.h</dt>
    <dd>This is a headder file.</dd>
  <dt>C05TriggerHole.C</dt>
    <dd>This is a source file.</dd>
  <dt>main.cxx</dt>
    <dd>This is main fuction.</dd>
  <dt>Condor.sub</dt>
    <dd>This is a job option.</dd>
  <dt>argument.txt</dt>
    <dd>This is an argument file.</dd>
</dl> 

### how to compile 
cd C05TriggerHole/
sh compile.sh

### how to check the L1TGCNtuple at CINT
root [0] TChain\* physics = new TChain("physics")
root [1] physics->Add( "file name" )
root [2] physics->Draw( "as you like" )

### how to submit Condor jobs 
condor_submit Condor.sub
