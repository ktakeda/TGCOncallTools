//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 15 13:26:23 2018 by ROOT version 5.34/38
// from TChain physics/
//////////////////////////////////////////////////////////

#ifndef C05TriggerHole_h
#define C05TriggerHole_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <iostream>

#include "Dict.cc"

// Fixed size dimensions of array or collections stored in the TTree if any.

class C05TriggerHole {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain


   // Declaration of leaf types
   // Offline muon
   Int_t  mu_n;
   std::vector<float>  *mu_pt;
   std::vector<float>  *mu_eta;
   std::vector<float>  *mu_phi;
   std::vector<float>  *mu_m;
   std::vector<int>  *mu_charge;
   std::vector<int>  *mu_author;
   std::vector<unsigned short>  *mu_allAuthors;
   std::vector<int>  *mu_muonType;
   std::vector<float>  *mu_etcone20;
   std::vector<float>  *mu_etcone30;
   std::vector<float>  *mu_etcone40;
   std::vector<float>  *mu_ptcone20;
   std::vector<float>  *mu_ptcone30;
   std::vector<float>  *mu_ptcone40;
   std::vector<float>  *mu_trackfitchi2;
   std::vector<float>  *mu_trackfitndof;
   std::vector<int>  *mu_isPassedMCP;
   std::vector<int>  *mu_quality;
   std::vector<float>  *mu_msInnerMatchChi2;
   std::vector<float>  *mu_msOuterMatchChi2;
   std::vector<int>  *mu_msInnerMatchDOF;
   std::vector<int>  *mu_msOuterMatchDOF;
   std::vector<int>  *mu_nOutliersOnTrack;
   std::vector<int>  *mu_nBLHits;
   std::vector<int>  *mu_nPixHits;
   std::vector<int>  *mu_nSCTHits;
   std::vector<int>  *mu_nTRTHits;
   std::vector<int>  *mu_nTRTHighTHits;
   std::vector<int>  *mu_nBLSharedHits;
   std::vector<int>  *mu_nPixSharedHits;
   std::vector<int>  *mu_nPixHoles;
   std::vector<int>  *mu_nSCTSharedHits;
   std::vector<int>  *mu_nSCTHoles;
   std::vector<int>  *mu_nTRTOutliers;
   std::vector<int>  *mu_nTRTHighTOutliers;
   std::vector<int>  *mu_nGangedPixels;
   std::vector<int>  *mu_nPixelDeadSensors;
   std::vector<int>  *mu_nSCTDeadSensors;
   std::vector<int>  *mu_nTRTDeadStraws;
   std::vector<int>  *mu_expectBLayerHit;
   std::vector<int>  *mu_nPrecisionLayers;
   std::vector<int>  *mu_nPrecisionHoleLayers;
   std::vector<int>  *mu_nPhiLayers;
   std::vector<int>  *mu_nPhiHoleLayers;
   std::vector<int>  *mu_nTrigEtaLayers;
   std::vector<int>  *mu_nTrigEtaHoleLayers;
   std::vector<int>  *mu_primarySector;
   std::vector<int>  *mu_secondarySector;
   std::vector<int>  *mu_nInnerSmallHits;
   std::vector<int>  *mu_nInnerLargeHits;
   std::vector<int>  *mu_nMiddleSmallHits;
   std::vector<int>  *mu_nMiddleLargeHits;
   std::vector<int>  *mu_nOuterSmallHits;
   std::vector<int>  *mu_nOuterLargeHits;
   std::vector<int>  *mu_nExtendedSmallHits;
   std::vector<int>  *mu_nExtendedLargeHits;
   std::vector<int>  *mu_nInnerSmallHoles;
   std::vector<int>  *mu_nInnerLargeHoles;
   std::vector<int>  *mu_nMiddleSmallHoles;
   std::vector<int>  *mu_nMiddleLargeHoles;
   std::vector<int>  *mu_nOuterSmallHoles;
   std::vector<int>  *mu_nOuterLargeHoles;
   std::vector<int>  *mu_nExtendedSmallHoles;
   std::vector<int>  *mu_nExtendedLargeHoles;
   std::vector<int>  *mu_nPhiLayer1Hits;
   std::vector<int>  *mu_nPhiLayer2Hits;
   std::vector<int>  *mu_nPhiLayer3Hits;
   std::vector<int>  *mu_nPhiLayer4Hits;
   std::vector<int>  *mu_nEtaLayer1Hits;
   std::vector<int>  *mu_nEtaLayer2Hits;
   std::vector<int>  *mu_nEtaLayer3Hits;
   std::vector<int>  *mu_nEtaLayer4Hits;
   std::vector<int>  *mu_nPhiLayer1Holes;
   std::vector<int>  *mu_nPhiLayer2Holes;
   std::vector<int>  *mu_nPhiLayer3Holes;
   std::vector<int>  *mu_nPhiLayer4Holes;
   std::vector<int>  *mu_nEtaLayer1Holes;
   std::vector<int>  *mu_nEtaLayer2Holes;
   std::vector<int>  *mu_nEtaLayer3Holes;
   std::vector<int>  *mu_nEtaLayer4Holes;

   // MuCTPi
   Int_t           trig_L1_mu_n;
   vector<float>   *trig_L1_mu_eta;
   vector<float>   *trig_L1_mu_phi;
   vector<string>  *trig_L1_mu_thrName;
   vector<short>   *trig_L1_mu_thrNumber;
   vector<short>   *trig_L1_mu_RoINumber;
   vector<short>   *trig_L1_mu_sectorAddress;
   vector<int>     *trig_L1_mu_firstCandidate;
   vector<int>     *trig_L1_mu_moreCandInRoI;
   vector<int>     *trig_L1_mu_moreCandInSector;
   vector<short>   *trig_L1_mu_source;
   vector<short>   *trig_L1_mu_hemisphere;
   vector<short>   *trig_L1_mu_charge;
   vector<int>     *trig_L1_mu_vetoed;
   
   // TGC_coin_XXX
   Int_t           TGC_coin_n;
   vector<float>   *TGC_coin_x_In;
   vector<float>   *TGC_coin_y_In;
   vector<float>   *TGC_coin_z_In;
   vector<float>   *TGC_coin_x_Out;
   vector<float>   *TGC_coin_y_Out;
   vector<float>   *TGC_coin_z_Out;
   vector<float>   *TGC_coin_width_In;
   vector<float>   *TGC_coin_width_Out;
   vector<float>   *TGC_coin_width_R;
   vector<float>   *TGC_coin_width_Phi;
   vector<int>     *TGC_coin_isAside;
   vector<int>     *TGC_coin_isForward;
   vector<int>     *TGC_coin_isStrip;
   vector<int>     *TGC_coin_isInner;
   vector<int>     *TGC_coin_isPositiveDeltaR;
   vector<int>     *TGC_coin_type;
   vector<int>     *TGC_coin_trackletId;
   vector<int>     *TGC_coin_trackletIdStrip;
   vector<int>     *TGC_coin_phi;
   vector<int>     *TGC_coin_roi;
   vector<int>     *TGC_coin_pt;
   vector<int>     *TGC_coin_delta;
   vector<int>     *TGC_coin_sub;
   vector<int>     *TGC_coin_veto;
   vector<int>     *TGC_coin_bunch;
   vector<int>     *TGC_coin_inner;
  
   // trigger_info_XXX
   Int_t  trigger_info_n;
   std::vector<std::string>  *trigger_info_chain;
   std::vector<int>  *trigger_info_isPassed;
   std::vector<int>  *trigger_info_nTracks;
   std::vector<std::vector<int> >  *trigger_info_typeVec;
   std::vector<std::vector<float> >  *trigger_info_ptVec;
   std::vector<std::vector<float> >  *trigger_info_etaVec;
   std::vector<std::vector<float> >  *trigger_info_phiVec;
   
   // Offline
   TBranch        *b_mu_n;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_author;   //!
   TBranch        *b_mu_allAuthors;   //!
   TBranch        *b_mu_muonType;   //!
   TBranch        *b_mu_etcone20;   //!
   TBranch        *b_mu_etcone30;   //!
   TBranch        *b_mu_etcone40;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_trackfitchi2;   //!
   TBranch        *b_mu_trackfitndof;   //!
   TBranch        *b_mu_isPassedMCP;   //!
   TBranch        *b_mu_quality;   //!
   TBranch        *b_mu_msInnerMatchChi2;   //!
   TBranch        *b_mu_msOuterMatchChi2;   //!
   TBranch        *b_mu_msInnerMatchDOF;   //!
   TBranch        *b_mu_msOuterMatchDOF;   //!
   TBranch        *b_mu_nOutliersOnTrack;   //!
   TBranch        *b_mu_nBLHits;   //!
   TBranch        *b_mu_nPixHits;   //!
   TBranch        *b_mu_nSCTHits;   //!
   TBranch        *b_mu_nTRTHits;   //!
   TBranch        *b_mu_nTRTHighTHits;   //!
   TBranch        *b_mu_nBLSharedHits;   //!
   TBranch        *b_mu_nPixSharedHits;   //!
   TBranch        *b_mu_nPixHoles;   //!
   TBranch        *b_mu_nSCTSharedHits;   //!
   TBranch        *b_mu_nSCTHoles;   //!
   TBranch        *b_mu_nTRTOutliers;   //!
   TBranch        *b_mu_nTRTHighTOutliers;   //!
   TBranch        *b_mu_nGangedPixels;   //!
   TBranch        *b_mu_nPixelDeadSensors;   //!
   TBranch        *b_mu_nSCTDeadSensors;   //!
   TBranch        *b_mu_nTRTDeadStraws;   //!
   TBranch        *b_mu_expectBLayerHit;   //!
   TBranch        *b_mu_nPrecisionLayers;   //!
   TBranch        *b_mu_nPrecisionHoleLayers;   //!
   TBranch        *b_mu_nPhiLayers;   //!
   TBranch        *b_mu_nPhiHoleLayers;   //!
   TBranch        *b_mu_nTrigEtaLayers;   //!
   TBranch        *b_mu_nTrigEtaHoleLayers;   //!
   TBranch        *b_mu_primarySector;   //!
   TBranch        *b_mu_secondarySector;   //!
   TBranch        *b_mu_nInnerSmallHits;   //!
   TBranch        *b_mu_nInnerLargeHits;   //!
   TBranch        *b_mu_nMiddleSmallHits;   //!
   TBranch        *b_mu_nMiddleLargeHits;   //!
   TBranch        *b_mu_nOuterSmallHits;   //!
   TBranch        *b_mu_nOuterLargeHits;   //!
   TBranch        *b_mu_nExtendedSmallHits;   //!
   TBranch        *b_mu_nExtendedLargeHits;   //!
   TBranch        *b_mu_nInnerSmallHoles;   //!
   TBranch        *b_mu_nInnerLargeHoles;   //!
   TBranch        *b_mu_nMiddleSmallHoles;   //!
   TBranch        *b_mu_nMiddleLargeHoles;   //!
   TBranch        *b_mu_nOuterSmallHoles;   //!
   TBranch        *b_mu_nOuterLargeHoles;   //!
   TBranch        *b_mu_nExtendedSmallHoles;   //!
   TBranch        *b_mu_nExtendedLargeHoles;   //!
   TBranch        *b_mu_nPhiLayer1Hits;   //!
   TBranch        *b_mu_nPhiLayer2Hits;   //!
   TBranch        *b_mu_nPhiLayer3Hits;   //!
   TBranch        *b_mu_nPhiLayer4Hits;   //!
   TBranch        *b_mu_nEtaLayer1Hits;   //!
   TBranch        *b_mu_nEtaLayer2Hits;   //!
   TBranch        *b_mu_nEtaLayer3Hits;   //!
   TBranch        *b_mu_nEtaLayer4Hits;   //!
   TBranch        *b_mu_nPhiLayer1Holes;   //!
   TBranch        *b_mu_nPhiLayer2Holes;   //!
   TBranch        *b_mu_nPhiLayer3Holes;   //!
   TBranch        *b_mu_nPhiLayer4Holes;   //!
   TBranch        *b_mu_nEtaLayer1Holes;   //!
   TBranch        *b_mu_nEtaLayer2Holes;   //!
   TBranch        *b_mu_nEtaLayer3Holes;   //!
   TBranch        *b_mu_nEtaLayer4Holes;   //!

   // List of branches
   TBranch        *b_trig_L1_mu_n;   //!
   TBranch        *b_trig_L1_mu_eta;   //!
   TBranch        *b_trig_L1_mu_phi;   //!
   TBranch        *b_trig_L1_mu_thrName;   //!
   TBranch        *b_trig_L1_mu_thrNumber;   //!
   TBranch        *b_trig_L1_mu_RoINumber;   //!
   TBranch        *b_trig_L1_mu_sectorAddress;   //!
   TBranch        *b_trig_L1_mu_firstCandidate;   //!
   TBranch        *b_trig_L1_mu_moreCandInRoI;   //!
   TBranch        *b_trig_L1_mu_moreCandInSector;   //!
   TBranch        *b_trig_L1_mu_source;   //!
   TBranch        *b_trig_L1_mu_hemisphere;   //!
   TBranch        *b_trig_L1_mu_charge;   //!
   TBranch        *b_trig_L1_mu_vetoed;   //!
   TBranch        *b_TGC_coin_n;   //!
   TBranch        *b_TGC_coin_x_In;   //!
   TBranch        *b_TGC_coin_y_In;   //!
   TBranch        *b_TGC_coin_z_In;   //!
   TBranch        *b_TGC_coin_x_Out;   //!
   TBranch        *b_TGC_coin_y_Out;   //!
   TBranch        *b_TGC_coin_z_Out;   //!
   TBranch        *b_TGC_coin_width_In;   //!
   TBranch        *b_TGC_coin_width_Out;   //!
   TBranch        *b_TGC_coin_width_R;   //!
   TBranch        *b_TGC_coin_width_Phi;   //!
   TBranch        *b_TGC_coin_isAside;   //!
   TBranch        *b_TGC_coin_isForward;   //!
   TBranch        *b_TGC_coin_isStrip;   //!
   TBranch        *b_TGC_coin_isInner;   //!
   TBranch        *b_TGC_coin_isPositiveDeltaR;   //!
   TBranch        *b_TGC_coin_type;   //!
   TBranch        *b_TGC_coin_trackletId;   //!
   TBranch        *b_TGC_coin_trackletIdStrip;   //!
   TBranch        *b_TGC_coin_phi;   //!
   TBranch        *b_TGC_coin_roi;   //!
   TBranch        *b_TGC_coin_pt;   //!
   TBranch        *b_TGC_coin_delta;   //!
   TBranch        *b_TGC_coin_sub;   //!
   TBranch        *b_TGC_coin_veto;   //!
   TBranch        *b_TGC_coin_bunch;   //!
   TBranch        *b_TGC_coin_inner;   //!
  
   TBranch        *b_trigger_info_n;   //!
   TBranch        *b_trigger_info_chain;   //!
   TBranch        *b_trigger_info_isPassed;   //!
   TBranch        *b_trigger_info_nTracks;   //!
   TBranch        *b_trigger_info_typeVec;   //!
   TBranch        *b_trigger_info_ptVec;   //!
   TBranch        *b_trigger_info_etaVec;   //!
   TBranch        *b_trigger_info_phiVec;   //!

   C05TriggerHole(TTree *tree=0);
   C05TriggerHole(const std::string&);
   virtual ~C05TriggerHole();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   std::string m_file;
};

#endif

#ifdef C05TriggerHole_cxx
C05TriggerHole::C05TriggerHole(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("physics",tree);

#else // SINGLE_TREE


      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("physics","");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000001.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000002.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000003.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000004.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000005.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000006.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000007.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000008.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000009.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000010.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000011.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000012.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000013.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000014.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000015.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000016.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000017.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000018.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000019.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000020.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000021.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000022.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000023.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000024.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000025.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000026.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000027.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000028.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000029.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000030.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000031.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000032.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000033.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000034.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000035.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000036.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000037.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000038.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000039.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000040.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000041.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000042.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000043.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000044.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000045.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000046.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000047.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000048.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000049.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000050.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000051.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000052.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000053.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000054.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000055.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000056.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000057.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000058.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000059.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000060.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000061.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000062.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000063.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000064.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000065.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000066.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000067.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000068.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000069.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000070.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000071.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000072.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000073.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000074.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000075.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000076.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000077.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000078.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000079.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000080.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000081.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000082.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000083.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000084.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000085.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000086.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000087.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000088.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000089.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000090.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000091.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000092.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000093.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000094.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000095.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000096.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000097.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000098.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000099.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000100.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000101.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000102.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000103.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000104.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000105.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000106.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000107.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000108.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000109.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000110.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000111.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000112.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000113.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000114.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000115.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000116.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000117.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000118.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000119.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000120.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000121.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000122.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000123.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000124.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000125.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000126.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000127.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000128.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000129.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000130.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000131.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000132.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000133.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000134.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000135.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000136.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000137.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000138.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000139.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000140.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000141.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000142.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000143.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000144.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000145.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000146.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000147.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000148.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000149.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000150.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000151.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000152.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000153.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000154.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000155.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000156.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000157.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000158.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000159.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000160.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000161.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000162.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000163.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000164.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000165.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000166.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000167.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000168.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000169.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000170.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000171.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000172.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000173.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000174.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000175.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000176.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000177.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000178.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000179.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000180.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000181.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000182.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000183.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000184.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000185.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000186.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000187.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000188.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000189.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000190.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000191.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000192.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000193.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000194.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000195.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000196.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000197.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000198.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000199.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000200.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000201.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000202.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000203.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000204.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000205.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000206.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000207.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000208.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000209.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000210.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000211.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000212.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000213.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000214.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000215.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000216.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000217.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000218.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000219.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000220.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000221.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000222.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000223.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000224.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000225.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000226.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000227.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000228.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000229.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000230.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000231.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000232.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000233.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000234.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000235.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000236.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000237.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000238.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000239.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000240.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000241.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000242.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000243.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000244.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000245.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000246.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000247.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000248.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000249.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000250.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000251.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000252.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000253.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000254.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000255.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000256.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000257.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000258.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000260.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000261.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000262.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000263.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000264.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000265.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000266.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000267.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000268.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000269.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000270.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000271.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000272.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000273.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000274.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000275.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000276.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000277.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000278.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000279.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000280.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000281.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000282.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000283.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000284.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000285.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000286.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000287.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000288.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000289.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000290.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000291.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000292.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000293.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000294.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000295.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000296.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000297.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000298.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000299.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000300.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000301.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000302.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000303.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000304.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000305.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000306.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000307.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000308.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000309.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000310.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000311.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000312.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000313.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000314.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000315.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000317.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000318.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000319.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000320.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000321.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000322.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000323.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000324.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000325.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000326.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000327.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000328.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000329.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000330.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000331.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000332.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000333.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000334.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000336.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000337.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000339.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000340.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000341.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000342.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000343.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000344.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000345.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000346.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000347.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000348.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000349.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000350.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000351.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000352.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000353.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000354.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000355.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000356.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000357.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000358.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000359.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000360.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000361.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000362.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000363.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000365.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000366.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000367.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000368.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000369.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000370.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000371.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000372.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000373.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000374.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000375.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000376.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000377.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000378.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000379.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000380.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000381.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000382.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000383.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000384.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000385.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000386.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000387.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000389.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000391.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000392.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000393.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000394.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000395.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000396.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000397.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000398.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000399.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000400.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000401.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000402.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000403.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000404.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000405.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000406.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000407.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000408.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000409.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000410.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000411.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000412.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000413.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000414.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000415.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000416.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000417.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000418.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000419.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000420.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000421.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000422.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000423.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000424.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000425.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000426.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000427.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000428.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000429.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000430.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000431.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000432.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000433.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000434.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000435.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000436.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000437.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000438.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000439.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000440.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000441.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000442.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000443.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000444.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000445.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000446.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000447.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000448.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000449.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000450.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000451.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000452.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000453.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000459.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000460.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000461.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000462.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000463.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000464.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000465.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000466.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000468.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000469.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000470.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000471.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000472.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000473.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000474.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000475.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000476.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000477.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000478.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000479.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000480.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000481.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000482.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000483.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000509.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000510.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000511.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000512.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000513.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000514.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000515.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000516.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000517.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000518.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000519.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000520.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000521.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000522.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000523.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000524.root/physics");
      chain->Add("group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/group.det-muon.14211126.L1TGCNtuple._000526.root/physics");
      tree = chain;
#endif // SINGLE_TREE

   }
   m_file = "L1TGCNtuple_2018";
   Init(tree);
}

C05TriggerHole::C05TriggerHole(const std::string& file) : fChain(0)
{
  m_file = file;
  
  std::string path = "/eos/user/k/ktakeda/workspace/RootData/L1TGCNtuple/group.det-muon.data18_13TeV.00350531.physics_Main.merge.NTUP_MCP.1.f934_m1964.00-01-02_L1TGCNtuple/";

  TChain * chain = new TChain("physics","");
  std::string input = path + file;
  chain->Add( input.c_str() );
  TTree* tree = chain;
  Init(tree);
}

C05TriggerHole::~C05TriggerHole()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t C05TriggerHole::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t C05TriggerHole::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void C05TriggerHole::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
  mu_n=0;
  mu_pt=0;
  mu_eta=0;
  mu_phi=0;
  mu_m=0;
  mu_charge=0;
  mu_author=0;
  mu_allAuthors=0;
  mu_muonType=0;
  mu_etcone20=0;
  mu_etcone30=0;
  mu_etcone40=0;
  mu_ptcone20=0;
  mu_ptcone30=0;
  mu_ptcone40=0;
  mu_trackfitchi2=0;
  mu_trackfitndof=0;
  mu_isPassedMCP=0;
  mu_quality=0;
  mu_msInnerMatchChi2=0;
  mu_msOuterMatchChi2=0;
  mu_msInnerMatchDOF=0;
  mu_msOuterMatchDOF=0;
  mu_nOutliersOnTrack=0;
  mu_nBLHits=0;
  mu_nPixHits=0;
  mu_nSCTHits=0;
  mu_nTRTHits=0;
  mu_nTRTHighTHits=0;
  mu_nBLSharedHits=0;
  mu_nPixSharedHits=0;
  mu_nPixHoles=0;
  mu_nSCTSharedHits=0;
  mu_nSCTHoles=0;
  mu_nTRTOutliers=0;
  mu_nTRTHighTOutliers=0;
  mu_nGangedPixels=0;
  mu_nPixelDeadSensors=0;
  mu_nSCTDeadSensors=0;
  mu_nTRTDeadStraws=0;
  mu_expectBLayerHit=0;
  mu_nPrecisionLayers=0;
  mu_nPrecisionHoleLayers=0;
  mu_nPhiLayers=0;
  mu_nPhiHoleLayers=0;
  mu_nTrigEtaLayers=0;
  mu_nTrigEtaHoleLayers=0;
  mu_primarySector=0;
  mu_secondarySector=0;
  mu_nInnerSmallHits=0;
  mu_nInnerLargeHits=0;
  mu_nMiddleSmallHits=0;
  mu_nMiddleLargeHits=0;
  mu_nOuterSmallHits=0;
  mu_nOuterLargeHits=0;
  mu_nExtendedSmallHits=0;
  mu_nExtendedLargeHits=0;
  mu_nInnerSmallHoles=0;
  mu_nInnerLargeHoles=0;
  mu_nMiddleSmallHoles=0;
  mu_nMiddleLargeHoles=0;
  mu_nOuterSmallHoles=0;
  mu_nOuterLargeHoles=0;
  mu_nExtendedSmallHoles=0;
  mu_nExtendedLargeHoles=0;
  mu_nPhiLayer1Hits=0;
  mu_nPhiLayer2Hits=0;
  mu_nPhiLayer3Hits=0;
  mu_nPhiLayer4Hits=0;
  mu_nEtaLayer1Hits=0;
  mu_nEtaLayer2Hits=0;
  mu_nEtaLayer3Hits=0;
  mu_nEtaLayer4Hits=0;
  mu_nPhiLayer1Holes=0;
  mu_nPhiLayer2Holes=0;
  mu_nPhiLayer3Holes=0;
  mu_nPhiLayer4Holes=0;
  mu_nEtaLayer1Holes=0;
  mu_nEtaLayer2Holes=0;
  mu_nEtaLayer3Holes=0;
  mu_nEtaLayer4Holes=0;
   trig_L1_mu_eta = 0;
   trig_L1_mu_phi = 0;
   trig_L1_mu_thrName = 0;
   trig_L1_mu_thrNumber = 0;
   trig_L1_mu_RoINumber = 0;
   trig_L1_mu_sectorAddress = 0;
   trig_L1_mu_firstCandidate = 0;
   trig_L1_mu_moreCandInRoI = 0;
   trig_L1_mu_moreCandInSector = 0;
   trig_L1_mu_source = 0;
   trig_L1_mu_hemisphere = 0;
   trig_L1_mu_charge = 0;
   trig_L1_mu_vetoed = 0;
   TGC_coin_x_In = 0;
   TGC_coin_y_In = 0;
   TGC_coin_z_In = 0;
   TGC_coin_x_Out = 0;
   TGC_coin_y_Out = 0;
   TGC_coin_z_Out = 0;
   TGC_coin_width_In = 0;
   TGC_coin_width_Out = 0;
   TGC_coin_width_R = 0;
   TGC_coin_width_Phi = 0;
   TGC_coin_isAside = 0;
   TGC_coin_isForward = 0;
   TGC_coin_isStrip = 0;
   TGC_coin_isInner = 0;
   TGC_coin_isPositiveDeltaR = 0;
   TGC_coin_type = 0;
   TGC_coin_trackletId = 0;
   TGC_coin_trackletIdStrip = 0;
   TGC_coin_phi = 0;
   TGC_coin_roi = 0;
   TGC_coin_pt = 0;
   TGC_coin_delta = 0;
   TGC_coin_sub = 0;
   TGC_coin_veto = 0;
   TGC_coin_bunch = 0;
   TGC_coin_inner = 0;

   // trigger_info_XXX
   trigger_info_n=0;
   trigger_info_chain=0;
   trigger_info_isPassed=0;
   trigger_info_nTracks=0;
   trigger_info_typeVec=0;
   trigger_info_ptVec=0;
   trigger_info_etaVec=0;
   trigger_info_phiVec=0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchStatus("*",0);
   fChain->SetBranchStatus("mu_*",1);
   fChain->SetBranchStatus("TGC_coin_*",1);
   fChain->SetBranchStatus("trig_L1_*",1);
   fChain->SetBranchStatus("trigger_*",1);
  
   // Offline muon
   fChain->SetBranchAddress("mu_n", &mu_n, &b_mu_n);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_allAuthors", &mu_allAuthors, &b_mu_allAuthors);
   fChain->SetBranchAddress("mu_muonType", &mu_muonType, &b_mu_muonType);
   fChain->SetBranchAddress("mu_etcone20", &mu_etcone20, &b_mu_etcone20);
   fChain->SetBranchAddress("mu_etcone30", &mu_etcone30, &b_mu_etcone30);
   fChain->SetBranchAddress("mu_etcone40", &mu_etcone40, &b_mu_etcone40);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_trackfitchi2", &mu_trackfitchi2, &b_mu_trackfitchi2);
   fChain->SetBranchAddress("mu_trackfitndof", &mu_trackfitndof, &b_mu_trackfitndof);
   fChain->SetBranchAddress("mu_isPassedMCP", &mu_isPassedMCP, &b_mu_isPassedMCP);
   fChain->SetBranchAddress("mu_quality", &mu_quality, &b_mu_quality);
   fChain->SetBranchAddress("mu_msInnerMatchChi2", &mu_msInnerMatchChi2, &b_mu_msInnerMatchChi2);
   fChain->SetBranchAddress("mu_msOuterMatchChi2", &mu_msOuterMatchChi2, &b_mu_msOuterMatchChi2);
   fChain->SetBranchAddress("mu_msInnerMatchDOF", &mu_msInnerMatchDOF, &b_mu_msInnerMatchDOF);
   fChain->SetBranchAddress("mu_msOuterMatchDOF", &mu_msOuterMatchDOF, &b_mu_msOuterMatchDOF);
   fChain->SetBranchAddress("mu_nOutliersOnTrack", &mu_nOutliersOnTrack, &b_mu_nOutliersOnTrack);
   fChain->SetBranchAddress("mu_nBLHits", &mu_nBLHits, &b_mu_nBLHits);
   fChain->SetBranchAddress("mu_nPixHits", &mu_nPixHits, &b_mu_nPixHits);
   fChain->SetBranchAddress("mu_nSCTHits", &mu_nSCTHits, &b_mu_nSCTHits);
   fChain->SetBranchAddress("mu_nTRTHits", &mu_nTRTHits, &b_mu_nTRTHits);
   fChain->SetBranchAddress("mu_nTRTHighTHits", &mu_nTRTHighTHits, &b_mu_nTRTHighTHits);
   fChain->SetBranchAddress("mu_nBLSharedHits", &mu_nBLSharedHits, &b_mu_nBLSharedHits);
   fChain->SetBranchAddress("mu_nPixSharedHits", &mu_nPixSharedHits, &b_mu_nPixSharedHits);
   fChain->SetBranchAddress("mu_nPixHoles", &mu_nPixHoles, &b_mu_nPixHoles);
   fChain->SetBranchAddress("mu_nSCTSharedHits", &mu_nSCTSharedHits, &b_mu_nSCTSharedHits);
   fChain->SetBranchAddress("mu_nSCTHoles", &mu_nSCTHoles, &b_mu_nSCTHoles);
   fChain->SetBranchAddress("mu_nTRTOutliers", &mu_nTRTOutliers, &b_mu_nTRTOutliers);
   fChain->SetBranchAddress("mu_nTRTHighTOutliers", &mu_nTRTHighTOutliers, &b_mu_nTRTHighTOutliers);
   fChain->SetBranchAddress("mu_nGangedPixels", &mu_nGangedPixels, &b_mu_nGangedPixels);
   fChain->SetBranchAddress("mu_nPixelDeadSensors", &mu_nPixelDeadSensors, &b_mu_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_nSCTDeadSensors", &mu_nSCTDeadSensors, &b_mu_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_nTRTDeadStraws", &mu_nTRTDeadStraws, &b_mu_nTRTDeadStraws);
   fChain->SetBranchAddress("mu_expectBLayerHit", &mu_expectBLayerHit, &b_mu_expectBLayerHit);
   fChain->SetBranchAddress("mu_nPrecisionLayers", &mu_nPrecisionLayers, &b_mu_nPrecisionLayers);
   fChain->SetBranchAddress("mu_nPrecisionHoleLayers", &mu_nPrecisionHoleLayers, &b_mu_nPrecisionHoleLayers);
   fChain->SetBranchAddress("mu_nPhiLayers", &mu_nPhiLayers, &b_mu_nPhiLayers);
   fChain->SetBranchAddress("mu_nPhiHoleLayers", &mu_nPhiHoleLayers, &b_mu_nPhiHoleLayers);
   fChain->SetBranchAddress("mu_nTrigEtaLayers", &mu_nTrigEtaLayers, &b_mu_nTrigEtaLayers);
   fChain->SetBranchAddress("mu_nTrigEtaHoleLayers", &mu_nTrigEtaHoleLayers, &b_mu_nTrigEtaHoleLayers);
   fChain->SetBranchAddress("mu_primarySector", &mu_primarySector, &b_mu_primarySector);
   fChain->SetBranchAddress("mu_secondarySector", &mu_secondarySector, &b_mu_secondarySector);
   fChain->SetBranchAddress("mu_nInnerSmallHits", &mu_nInnerSmallHits, &b_mu_nInnerSmallHits);
   fChain->SetBranchAddress("mu_nInnerLargeHits", &mu_nInnerLargeHits, &b_mu_nInnerLargeHits);
   fChain->SetBranchAddress("mu_nMiddleSmallHits", &mu_nMiddleSmallHits, &b_mu_nMiddleSmallHits);
   fChain->SetBranchAddress("mu_nMiddleLargeHits", &mu_nMiddleLargeHits, &b_mu_nMiddleLargeHits);
   fChain->SetBranchAddress("mu_nOuterSmallHits", &mu_nOuterSmallHits, &b_mu_nOuterSmallHits);
   fChain->SetBranchAddress("mu_nOuterLargeHits", &mu_nOuterLargeHits, &b_mu_nOuterLargeHits);
   fChain->SetBranchAddress("mu_nExtendedSmallHits", &mu_nExtendedSmallHits, &b_mu_nExtendedSmallHits);
   fChain->SetBranchAddress("mu_nExtendedLargeHits", &mu_nExtendedLargeHits, &b_mu_nExtendedLargeHits);
   fChain->SetBranchAddress("mu_nInnerSmallHoles", &mu_nInnerSmallHoles, &b_mu_nInnerSmallHoles);
   fChain->SetBranchAddress("mu_nInnerLargeHoles", &mu_nInnerLargeHoles, &b_mu_nInnerLargeHoles);
   fChain->SetBranchAddress("mu_nMiddleSmallHoles", &mu_nMiddleSmallHoles, &b_mu_nMiddleSmallHoles);
   fChain->SetBranchAddress("mu_nMiddleLargeHoles", &mu_nMiddleLargeHoles, &b_mu_nMiddleLargeHoles);
   fChain->SetBranchAddress("mu_nOuterSmallHoles", &mu_nOuterSmallHoles, &b_mu_nOuterSmallHoles);
   fChain->SetBranchAddress("mu_nOuterLargeHoles", &mu_nOuterLargeHoles, &b_mu_nOuterLargeHoles);
   fChain->SetBranchAddress("mu_nExtendedSmallHoles", &mu_nExtendedSmallHoles, &b_mu_nExtendedSmallHoles);
   fChain->SetBranchAddress("mu_nExtendedLargeHoles", &mu_nExtendedLargeHoles, &b_mu_nExtendedLargeHoles);
   fChain->SetBranchAddress("mu_nPhiLayer1Hits", &mu_nPhiLayer1Hits, &b_mu_nPhiLayer1Hits);
   fChain->SetBranchAddress("mu_nPhiLayer2Hits", &mu_nPhiLayer2Hits, &b_mu_nPhiLayer2Hits);
   fChain->SetBranchAddress("mu_nPhiLayer3Hits", &mu_nPhiLayer3Hits, &b_mu_nPhiLayer3Hits);
   fChain->SetBranchAddress("mu_nPhiLayer4Hits", &mu_nPhiLayer4Hits, &b_mu_nPhiLayer4Hits);
   fChain->SetBranchAddress("mu_nEtaLayer1Hits", &mu_nEtaLayer1Hits, &b_mu_nEtaLayer1Hits);
   fChain->SetBranchAddress("mu_nEtaLayer2Hits", &mu_nEtaLayer2Hits, &b_mu_nEtaLayer2Hits);
   fChain->SetBranchAddress("mu_nEtaLayer3Hits", &mu_nEtaLayer3Hits, &b_mu_nEtaLayer3Hits);
   fChain->SetBranchAddress("mu_nEtaLayer4Hits", &mu_nEtaLayer4Hits, &b_mu_nEtaLayer4Hits);
   fChain->SetBranchAddress("mu_nPhiLayer1Holes", &mu_nPhiLayer1Holes, &b_mu_nPhiLayer1Holes);
   fChain->SetBranchAddress("mu_nPhiLayer2Holes", &mu_nPhiLayer2Holes, &b_mu_nPhiLayer2Holes);
   fChain->SetBranchAddress("mu_nPhiLayer3Holes", &mu_nPhiLayer3Holes, &b_mu_nPhiLayer3Holes);
   fChain->SetBranchAddress("mu_nPhiLayer4Holes", &mu_nPhiLayer4Holes, &b_mu_nPhiLayer4Holes);
   fChain->SetBranchAddress("mu_nEtaLayer1Holes", &mu_nEtaLayer1Holes, &b_mu_nEtaLayer1Holes);
   fChain->SetBranchAddress("mu_nEtaLayer2Holes", &mu_nEtaLayer2Holes, &b_mu_nEtaLayer2Holes);
   fChain->SetBranchAddress("mu_nEtaLayer3Holes", &mu_nEtaLayer3Holes, &b_mu_nEtaLayer3Holes);
   fChain->SetBranchAddress("mu_nEtaLayer4Holes", &mu_nEtaLayer4Holes, &b_mu_nEtaLayer4Holes);

   fChain->SetBranchAddress("trig_L1_mu_n", &trig_L1_mu_n, &b_trig_L1_mu_n);
   fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
   fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
   fChain->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName, &b_trig_L1_mu_thrName);
   fChain->SetBranchAddress("trig_L1_mu_thrNumber", &trig_L1_mu_thrNumber, &b_trig_L1_mu_thrNumber);
   fChain->SetBranchAddress("trig_L1_mu_RoINumber", &trig_L1_mu_RoINumber, &b_trig_L1_mu_RoINumber);
   fChain->SetBranchAddress("trig_L1_mu_sectorAddress", &trig_L1_mu_sectorAddress, &b_trig_L1_mu_sectorAddress);
   fChain->SetBranchAddress("trig_L1_mu_firstCandidate", &trig_L1_mu_firstCandidate, &b_trig_L1_mu_firstCandidate);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInRoI", &trig_L1_mu_moreCandInRoI, &b_trig_L1_mu_moreCandInRoI);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInSector", &trig_L1_mu_moreCandInSector, &b_trig_L1_mu_moreCandInSector);
   fChain->SetBranchAddress("trig_L1_mu_source", &trig_L1_mu_source, &b_trig_L1_mu_source);
   fChain->SetBranchAddress("trig_L1_mu_hemisphere", &trig_L1_mu_hemisphere, &b_trig_L1_mu_hemisphere);
   fChain->SetBranchAddress("trig_L1_mu_charge", &trig_L1_mu_charge, &b_trig_L1_mu_charge);
   fChain->SetBranchAddress("trig_L1_mu_vetoed", &trig_L1_mu_vetoed, &b_trig_L1_mu_vetoed);
   fChain->SetBranchAddress("TGC_coin_n", &TGC_coin_n, &b_TGC_coin_n);
   fChain->SetBranchAddress("TGC_coin_x_In", &TGC_coin_x_In, &b_TGC_coin_x_In);
   fChain->SetBranchAddress("TGC_coin_y_In", &TGC_coin_y_In, &b_TGC_coin_y_In);
   fChain->SetBranchAddress("TGC_coin_z_In", &TGC_coin_z_In, &b_TGC_coin_z_In);
   fChain->SetBranchAddress("TGC_coin_x_Out", &TGC_coin_x_Out, &b_TGC_coin_x_Out);
   fChain->SetBranchAddress("TGC_coin_y_Out", &TGC_coin_y_Out, &b_TGC_coin_y_Out);
   fChain->SetBranchAddress("TGC_coin_z_Out", &TGC_coin_z_Out, &b_TGC_coin_z_Out);
   fChain->SetBranchAddress("TGC_coin_width_In", &TGC_coin_width_In, &b_TGC_coin_width_In);
   fChain->SetBranchAddress("TGC_coin_width_Out", &TGC_coin_width_Out, &b_TGC_coin_width_Out);
   fChain->SetBranchAddress("TGC_coin_width_R", &TGC_coin_width_R, &b_TGC_coin_width_R);
   fChain->SetBranchAddress("TGC_coin_width_Phi", &TGC_coin_width_Phi, &b_TGC_coin_width_Phi);
   fChain->SetBranchAddress("TGC_coin_isAside", &TGC_coin_isAside, &b_TGC_coin_isAside);
   fChain->SetBranchAddress("TGC_coin_isForward", &TGC_coin_isForward, &b_TGC_coin_isForward);
   fChain->SetBranchAddress("TGC_coin_isStrip", &TGC_coin_isStrip, &b_TGC_coin_isStrip);
   fChain->SetBranchAddress("TGC_coin_isInner", &TGC_coin_isInner, &b_TGC_coin_isInner);
   fChain->SetBranchAddress("TGC_coin_isPositiveDeltaR", &TGC_coin_isPositiveDeltaR, &b_TGC_coin_isPositiveDeltaR);
   fChain->SetBranchAddress("TGC_coin_type", &TGC_coin_type, &b_TGC_coin_type);
   fChain->SetBranchAddress("TGC_coin_trackletId", &TGC_coin_trackletId, &b_TGC_coin_trackletId);
   fChain->SetBranchAddress("TGC_coin_trackletIdStrip", &TGC_coin_trackletIdStrip, &b_TGC_coin_trackletIdStrip);
   fChain->SetBranchAddress("TGC_coin_phi", &TGC_coin_phi, &b_TGC_coin_phi);
   fChain->SetBranchAddress("TGC_coin_roi", &TGC_coin_roi, &b_TGC_coin_roi);
   fChain->SetBranchAddress("TGC_coin_pt", &TGC_coin_pt, &b_TGC_coin_pt);
   fChain->SetBranchAddress("TGC_coin_delta", &TGC_coin_delta, &b_TGC_coin_delta);
   fChain->SetBranchAddress("TGC_coin_sub", &TGC_coin_sub, &b_TGC_coin_sub);
   fChain->SetBranchAddress("TGC_coin_veto", &TGC_coin_veto, &b_TGC_coin_veto);
   fChain->SetBranchAddress("TGC_coin_bunch", &TGC_coin_bunch, &b_TGC_coin_bunch);
   fChain->SetBranchAddress("TGC_coin_inner", &TGC_coin_inner, &b_TGC_coin_inner);
   
   fChain->SetBranchAddress("trigger_info_n", &trigger_info_n, &b_trigger_info_n);
   fChain->SetBranchAddress("trigger_info_chain", &trigger_info_chain, &b_trigger_info_chain);
   fChain->SetBranchAddress("trigger_info_isPassed", &trigger_info_isPassed, &b_trigger_info_isPassed);
   fChain->SetBranchAddress("trigger_info_nTracks", &trigger_info_nTracks, &b_trigger_info_nTracks);
   fChain->SetBranchAddress("trigger_info_typeVec", &trigger_info_typeVec, &b_trigger_info_typeVec);
   fChain->SetBranchAddress("trigger_info_ptVec", &trigger_info_ptVec, &b_trigger_info_ptVec);
   fChain->SetBranchAddress("trigger_info_etaVec", &trigger_info_etaVec, &b_trigger_info_etaVec);
   fChain->SetBranchAddress("trigger_info_phiVec", &trigger_info_phiVec, &b_trigger_info_phiVec);
   
   Notify();
}

Bool_t C05TriggerHole::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void C05TriggerHole::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t C05TriggerHole::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef C05TriggerHole_cxx
