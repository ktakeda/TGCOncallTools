
# C05TriggerHole
## The descriptions for each file
* Submit.sh
  * You can submit a job to the grid system. (sh Submit.sh <argument1> <argument2>)
  * argument1 : input availability file
  * argument2 : outDS container version name

* mainLocal.cxx
* mainGrid.cxx
  * These files are the main function for condor and grid environment respectively.
  
* C05 trigger hole investigation logics are written in the follwoing code.
  * C05TriggerHole.h
  * C05TriggerHole.C
  * Dict.cc
  * Dict.h

* To compile the code 
  * compileLocal.sh
    * This local means for the HTCondor job.
  * compileGrid.sh

* Option files  
  * avaliability.txt
    * This is the same as a file https://gitlab.cern.ch/atlas-l1muon/endcap/L1TGCNtuple/blob/master/grid/availability.txt
  * argument.txt
    * This is an option file for the HTCondor jobs.
  * execute.sh
  * Condor.sub

## Submit a condor job
condor\_submit Condor.sub
### Note!!
If you want to submit jobs via HTCondor service, you have to download L1TGCNtuple files to your local environment.
And you should change the path to those local data. (See the C05TriggerHole.h, and should modifiy the C05TriggerHole constructer.)

## Submit a grid job
sh Submit.sh <argument1> <argument2>

## Test a job at the local environment.
./mainLocal group.det-muon.14211126.L1TGCNtuple.\_000477.root
This function use a path to /eos/user/k/ktakeda~.
