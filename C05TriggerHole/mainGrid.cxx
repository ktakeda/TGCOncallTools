
#include "C05TriggerHole.C"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
  std::cout << " C05TriggerHole::main() will be started. ------ " << std::endl;
  
  // read a string via file since long string causes memory error in CINT when it is read via stdin
  // For Grid system
  bool isGrid = false;
  std::string argStr;
  std::ifstream ifs("GridInput.txt");
  if ( !ifs.is_open() ) {
    std::cout << " main() can't open the GridInput.txt.... " << std::endl;
  }
  std::getline(ifs,argStr);

  // split by ','
  std::vector<std::string> fileList;
  for (size_t i=0,n; i <= argStr.length(); i=n+1)
  {
    n = argStr.find_first_of(',',i);
    if (n == string::npos)
      n = argStr.length();
    string tmp = argStr.substr(i,n-i);
    std::cout << " C05TriggerHole::main() will process : " << tmp << std::endl;
    fileList.push_back(tmp);
  }

  // open input files
  TChain fChain("physics");
  for (int iFile=0; iFile<fileList.size(); ++iFile)
  {
    std::cout << " C05TriggerHole::main() TChain::Add() " << fileList[iFile].c_str() << std::endl;
    fChain.Add(fileList[iFile].c_str());
  }
  
  C05TriggerHole t( &fChain );
  t.Loop();

  return 0;
}
