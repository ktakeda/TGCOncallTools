
#include <TFile.h>
#include <TH2.h>

#include <fstream>
#include <iostream>
#include <bitset>

void BigStormFinder()
{
  std::string path = "/eos/atlas/atlascerngroupdisk/tdaq-mon/coca/2018/TGCGnam/";

  // Input data path
  std::ifstream ifs("./TGCGnamDataSet.txt");
  if( !ifs ){
    std::cout << "Failed to open an input file." << std::endl;
    return;
  }

  // Open the output files
  std::ofstream ofs("./BigStormRunNumber.txt");
  if( !ofs ){
    std::cout << "Failed to open an output file." << std::endl;
    return;
  }

  std::string Side[2] = {"A","C"};
  TH2D* h_BigStorm[2][12];
  TH2* h_BigStorm_Rebin[2][12];
  
  ////////////////////////////////////////////////////
  //
  //  Create histograms
  //
  ///////////////////////////////////////////////////

  TH2D* h_SectorBySSW = new TH2D("h_SectorBySSW",
                                 "Big storm",
                                 24,0,24, // x-axis : C-Side, A-Side
                                 10,0,10); // y-axis : SSW 0 ~ 9
  
  TH2D* h_SectorByRunNumber = new TH2D("h_SectorByRunNumber",
                                       "Big storm",
                                       24,0,24, // x-axis : C-Side, A-Side
                                       353,0,353); // y-axis : Run number
  
  /// Category 
  TH2D* h_CategorizePerSector = new TH2D("h_CategorizePerSector",
                                         "Big storm",
                                         24, 0,24, // x-axis : C-Side, A-Side
                                         18, 0,18); // y-axis : the number of categories

  // Categorize per Sector per Side
  TH2D* h_CategoryByRunNumber[2][12];
  for (int iSide=0; iSide<2; iSide++){
    for (int iSector=0; iSector<12; iSector++){
      h_CategoryByRunNumber[iSide][iSector] = new TH2D(Form("h_CategoryByRunNumber_%sSide%02d", Side[iSide].c_str(), iSector+1),
                                                            "",
                                                            6, 0.5, 6.5, // the number of category
                                                            353,0.5,353.5);
    }
  }

  // Categorize per Sector
  TH2D* h_CategoryByRunNumberPerSector[12];
  for (int iSector=0; iSector<12; iSector++){
    h_CategoryByRunNumberPerSector[iSector] = new TH2D(Form("h_CategoryByRunNumber_Sector%02d", iSector+1),
                                                   "",
                                                   12, 0.5, 12.5, // the number of sectors
                                                   353,0.5,353.5);
  }

  // Categorize 
  TH2D* h_CategorizeDetailes[2][12];
  for (int iSide=0; iSide<2; iSide++){
    for (int iSector=0; iSector<12; iSector++){
      h_CategorizeDetailes[iSide][iSector] = new TH2D(Form("h_CategorizeDetailes_%s%02d", Side[iSide].c_str(), iSector+1),
                                                           "",
                                                           30, 0, 30, // Lumi block
                                                           10, 0, 10);// SSW
    }
  }

  ////////////////////////////////////////////////////
  //
  //  Create a ROOT output file
  //
  ///////////////////////////////////////////////////
  TFile* pRoot = new TFile("BigStorm.root","recreate");
  for ( int iSide=0; iSide<2; iSide++){
    pRoot->mkdir(Form("%sSide", Side[iSide].c_str()));
    pRoot->cd(Form("%sSide", Side[iSide].c_str()));
    for(int iSector=1; iSector<=12; iSector++){
      pRoot->mkdir(Form("%sSide/%s%02d", Side[iSide].c_str(), Side[iSide].c_str(), iSector));
      //pRoot->mkdir(Form("%s%02d", Side[iSide].c_str(), iSector));
    }
    pRoot->cd("..");
  }

  std::string line;
  std::vector<std::string> RunNumber;
  TFile* pFile = NULL;
  int iRunNumber=1;
  int ASide_Storms=0;
  int CSide_Storms=0;

//  h_CategorizePerSector->Fill(12,0);
//  h_CategorizePerSector->Fill(1,1);
//  h_CategorizePerSector->Fill(2,2);
//#ifdef NOTUSED
  Int_t Index[2][12] = {0};
  while(getline(ifs, line)){

    std::string fullpath = "root://eosatlas.cern.ch/" + path + line;
    RunNumber.push_back(line.substr(5,6));

    std::cout << fullpath << std::endl;
    pFile = TFile::Open(fullpath.c_str());

    std::string pathToBigStorm = "Histogramming-tgc/TGCGnamDaqErsHisto/SHIFT/TGCDaqErs";

    /// Get TObject from the files
    for ( int iSide=0; iSide<2; iSide++){
      pRoot->cd(Form("%sSide", Side[iSide].c_str()));
      for ( int iSector=0; iSector<12; iSector++){
        h_BigStorm[iSide][iSector] = (TH2D*)pFile->Get( Form ("Histogramming-tgc/TGCGnamDaqErsHisto/SHIFT/TGCDaqErs/BigStorm_%s%02d", Side[iSide].c_str(), iSector+1 )); 

        pRoot->cd(Form("%sSide/%s%02d", Side[iSide].c_str(), Side[iSide].c_str(), iSector+1));
        h_BigStorm[iSide][iSector]->Write( Form("BigStorm_%s%02d_%s", Side[iSide].c_str(), iSector+1, line.c_str()  ));
        pRoot->cd("..");
      }
      pRoot->cd("..");
    }
    
    // -- Variable declaretion ------------------
    Int_t x_bin;
    Int_t y_bin;
    Int_t BigStormCandidates[10] = {0};
    Int_t BigStormVeto[10] = {0};
    std::bitset<10> SSW;
    
    /// Count the number of big storms
    for ( int iSide =0; iSide < 2; iSide++){
      for ( int iSector=0; iSector< 12; iSector++){
        // Store the number of x and y bins
        //  -- ROOT bin -------------------------------------
        //  TH1D *h1 = new TH1D("name","title",nbins,xmin,xmax);
        //  0          : underflow bin
        //  1 ~ nbins  : ordinary bins
        //  nbins+1    : overflow bin
        x_bin = h_BigStorm[iSide][iSector]->GetNbinsX();
        y_bin = h_BigStorm[iSide][iSector]->GetNbinsY();
        // Initialization
        for (int iInit=0; iInit<10; iInit++){
          BigStormVeto[iInit] = 9999;
        }

        for( int iLumiBlockBin=1; iLumiBlockBin<=x_bin; iLumiBlockBin++) {
          
          // Initialization
          for (int iInit=0; iInit<10; iInit++){
            BigStormCandidates[iInit] = 0;
          }
          SSW.reset();

          ////////////////////////////////////////////////////
          //
          //  Count the number of SSWs per a lumi block.
          //
          ///////////////////////////////////////////////////
          for ( int iSSW=0; iSSW<=9; iSSW++ ) {
            int SSWbinNum = iSSW + 1; // the number of bin is 1 ~ 10 (note: SSW 0 ~ 9)
            BigStormCandidates[iSSW] = h_BigStorm[iSide][iSector]->GetBinContent(h_BigStorm[iSide][iSector]->GetBin(iLumiBlockBin, SSWbinNum));
            
            // If the big storm occurs within 10 Lumi block, 
            // such event won't be considered.
            if (BigStormCandidates[iSSW] > 0 ){
              if( BigStormVeto[iSSW] - iLumiBlockBin > 10 ) {
                SSW[iSSW] = 1;
                BigStormVeto[iSSW] = iLumiBlockBin;
              }
            }
          }// iSSW

          ////////////////////////////////////////////////////
          //
          //  Overview histograms
          //    - SSW VS Sector
          //    - Run number VS Sector
          //   
          //  ASide_Storms and CSide_Storms are the counter 
          //  which show the number of SSWs outputting the big storm events. 
          //
          ///////////////////////////////////////////////////
          Bool_t isBigStorm = false;
          for ( int iSSW=0; iSSW<10; iSSW++){
            if( SSW[iSSW] ) {
              if(iSide == 0 ) {
                h_SectorBySSW->Fill(iSector, iSSW);
                h_SectorByRunNumber->Fill(iSector, iRunNumber);
                ASide_Storms++;
              }
              if(iSide == 1 ) {
                h_SectorBySSW->Fill(iSector+12, iSSW);
                h_SectorByRunNumber->Fill(iSector+12, iRunNumber);
                CSide_Storms++;
              }
              h_CategorizeDetailes[iSide][iSector]->Fill(Index[iSide][iSector], iSSW);
              isBigStorm = true;
            }
          }
          if( isBigStorm ) Index[iSide][iSector] += 1;
          
          ////////////////////////////////////////////////////
          //
          //  The new histograms
          //   - To understand the errors more deeply, some categorization has been created.
          //   - Single-big storm event
          //   - Multi-big storm event
          //
          ///////////////////////////////////////////////////
          // Single burst
          Int_t iCategory=0;
          if ( SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && SSW[6] && !SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && SSW[7] && !SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && SSW[8] && !SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( !SSW[0] && !SSW[1] && !SSW[2] && !SSW[3] && !SSW[4] && !SSW[5] && !SSW[6] && !SSW[7] && !SSW[8] && SSW[9] ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }

          // Multi burst
          iCategory++;
          if ( SSW.to_string() == "0000000101" ){ // only SSW0 + SSW2
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( SSW.to_string() == "0000001111" ){ // only SSW0 + SSW1 + SSW2 + SSW3
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( SSW.to_string() == "0010011000" ){ // only SSW3 + SSW4 + SSW7
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( SSW.to_string() == "0011101111" ){ // only SSW0~3 + SSW5~7 
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          if ( SSW.to_string() == "0011111111" ){ // only SSW0 ~ SSW7 
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          //if ( SSW[0] && SSW[1] && SSW[2] && SSW[3] && SSW[4] && SSW[5] && SSW[6] && SSW[7] && SSW[9]){
          if ( SSW.to_string() == "1011111111" ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          //if ( SSW[0] && SSW[1] && SSW[2] && SSW[3] && SSW[4] && SSW[5] && SSW[6] && SSW[7] && SSW[8] && SSW[9]){
          if ( SSW.to_string() == "1111111111" ){
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);
          }
          iCategory++;
          //if ( SSW[0] || SSW[1] || SSW[2] || SSW[3] || SSW[4] || SSW[5] || SSW[6] || SSW[7] || SSW[8] || SSW[9] ){
          if ( SSW.any() ) {
            if ( iSide == 0 ) h_CategorizePerSector->Fill(iSector, iCategory);
            if ( iSide == 1 ) h_CategorizePerSector->Fill(iSector+12, iCategory);

            ofs << iSide << " " << iSector << " " << RunNumber.back() << "\n";
          }

        }// iLumiBlockBin
      } // iSector
    } // iSide
    iRunNumber++;
    pFile->Close();
    delete pFile;
//    break;
  }
//#endif

  std::cout << "Storm Num :  " <<  ASide_Storms << " " <<  CSide_Storms << std::endl;


  //TLine* line = new TLine( 12.5, -0.5, 12.5, 9.5);
  //line->SetLineColor(kBlue);
  //line->Draw("same");



  // h_SectorBySSW->GetXaxis()->SetLabelOffset(100);
  // h_SectorBySSW->GetXaxis()->SetNdivisions(3,1,1);
  // TString LabelText[24] = {"A01","A02","A03","A04","A05","A06","A07","A08","A09","A10","A11","A12",
  //                          "C01","C02","C03","C04","C05","C06","C07","C08","C09","C10","C11","C12" };
  //

  /// Set Labels for h_SectorBySSW
  for ( int iSide =1; iSide<=12; iSide++){
    h_SectorBySSW->GetXaxis()->SetBinLabel(iSide,Form("A%02d", iSide));
  }
  for ( int iSide =1; iSide<=12; iSide++){
    h_SectorBySSW->GetXaxis()->SetBinLabel(iSide+12,Form("C%02d", iSide));
  }
  for ( int iSSW =0; iSSW<=9; iSSW++){
    h_SectorBySSW->GetYaxis()->SetBinLabel(iSSW+1,Form("SSW%d", iSSW));
  }
  
  /// Set Labels for h_SectorByRunNumber
  for ( int iSide =1; iSide<=12; iSide++){
    h_SectorByRunNumber->GetXaxis()->SetBinLabel(iSide,Form("A%02d", iSide));
  }
  for ( int iSide =1; iSide<=12; iSide++){
    h_SectorByRunNumber->GetXaxis()->SetBinLabel(iSide+12,Form("C%02d", iSide));
  }
  for ( int irun=0; irun< RunNumber.size(); irun++){
    h_SectorByRunNumber->GetYaxis()->SetBinLabel(irun+1,Form("%s", RunNumber.at(irun).c_str()));
    h_SectorByRunNumber->GetYaxis()->SetLabelSize(0.01);
  }
  
  /// Set Labels for h_CategorizePerSector
  for ( int iSide =1; iSide<=12; iSide++){
    h_CategorizePerSector->GetXaxis()->SetBinLabel(iSide,Form("A%02d", iSide));
  }
  for ( int iSide =1; iSide<=12; iSide++){
    h_CategorizePerSector->GetXaxis()->SetBinLabel(iSide+12,Form("C%02d", iSide));
  }
  std::vector<std::string> CategoryName; 
  CategoryName.push_back("SSW0");
  CategoryName.push_back("SSW1");
  CategoryName.push_back("SSW2");
  CategoryName.push_back("SSW3");
  CategoryName.push_back("SSW4");
  CategoryName.push_back("SSW5");
  CategoryName.push_back("SSW6");
  CategoryName.push_back("SSW7");
  CategoryName.push_back("SSW8");
  CategoryName.push_back("SSW9");
  CategoryName.push_back("SSW0+2");
  CategoryName.push_back("SSW0~3");
  CategoryName.push_back("SSW3+4+7");
  CategoryName.push_back("SSW0~3+SSW5~7");
  CategoryName.push_back("SSW0~7");
  CategoryName.push_back("SSW0~7+SSW9");
  CategoryName.push_back("all");
  CategoryName.push_back("Any SSWs (0~9)");
  for ( int iCategory=0; iCategory<CategoryName.size(); iCategory++){
    h_CategorizePerSector->GetYaxis()->SetBinLabel(iCategory+1,Form("%s", CategoryName.at(iCategory).c_str()));
    h_CategorizePerSector->GetYaxis()->SetLabelSize(0.01);
  }
  h_CategorizePerSector->GetXaxis()->LabelsOption("v"); // Rotate x-axis
  
  /// Set Labels for h_CategoryByRunNumber
  for ( int iSector=0; iSector<12; iSector++){
    for ( int iCategory=1; iCategory<=12; iCategory++){
      if ( iCategory <=6 )
        h_CategoryByRunNumberPerSector[iSector]->GetXaxis()->SetBinLabel(iCategory, Form("Cat%d", iCategory) );
      if ( 6 < iCategory  )
        h_CategoryByRunNumberPerSector[iSector]->GetXaxis()->SetBinLabel(iCategory, Form("Cat%d", iCategory-6) );
    }
  }

  h_SectorBySSW->GetXaxis()->LabelsOption("v"); // Rotate x-axis
  h_SectorBySSW->Write();
  
  h_SectorByRunNumber->GetXaxis()->LabelsOption("v"); // Rotate x-axis
  h_SectorByRunNumber->Write();

  pRoot->cd();
  pRoot->mkdir("BigStorm");
  pRoot->cd("BigStorm");
  for( int iSide=0; iSide<2; iSide++){
    for( int iSector=0; iSector<12; iSector++){
      h_CategoryByRunNumber[iSide][iSector]->Write();
    }
  }
  for( int iSector=0; iSector<12; iSector++){
    h_CategoryByRunNumberPerSector[iSector]->LabelsOption("v");
    h_CategoryByRunNumberPerSector[iSector]->Write();
  }
  
  /// h_CategorizeDetailes
  for( int iSide=0; iSide<2; iSide++){
    for ( int iSector=0; iSector<12; iSector++){
      for( int iSSW=0; iSSW<=9; iSSW++){
        h_CategorizeDetailes[iSide][iSector]->GetYaxis()->SetBinLabel(iSSW+1,Form("SSW%d", iSSW));
      }
      h_CategorizeDetailes[iSide][iSector]->Write();
    }
  }
  
  h_CategorizePerSector->Write();

  pRoot->Write();
  pRoot->Close();
  ofs.close();

  return;
}
